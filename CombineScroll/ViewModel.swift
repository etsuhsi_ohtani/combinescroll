//
//  ViewModel.swift
//  CombineScroll
//
//  Created by Etsushi Otani on 2021/09/01.
//

import Foundation
import Combine

class ViewModel {
    let subject = PassthroughSubject<String, Never>()
    
    func call() {
        subject.send("ViewModel.call()")
        print("hogehoge")
    }
}

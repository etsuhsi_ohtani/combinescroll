//
//  ViewController.swift
//  CombineScroll
//
//  Created by Etsushi Otani on 2021/09/01.
//

import UIKit
import Combine

class ViewController: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    
    private let viewModel = ViewModel()
    private var cancell: AnyCancellable?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        scrollView.delegate = self
        cancell = viewModel.subject.sink(receiveValue: { str in
            // UIの更新など
            print(str)
            print(Date())
        })
    }
}

extension ViewController: UIScrollViewDelegate {
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        // スクロール開始
        print(Date())
        viewModel.call()
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        // スクロール中
        viewModel.call()
    }
}
